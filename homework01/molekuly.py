#!/usr/bin/env python3

"""
Zahrejte si na molekulu: Vystartujte z náhodného místa konzole náhodným směrem a na okrajích konzole se odrážejte podle (ideálního) zákona odrazu.

Řešte výše uvedenou úlohu z „Hrátek s printem“, tedy Pohyb molekuly v krabici, s následujícími zesložitěními:

 * vykreslete okraje „krabice“ (tedy terminálu) a pohybujte se pouze mezi nimi;
 * náraz molekuly do stěny vhodně zvýrazněte;
 * nechte krabicí létat přinejmenším dvě molekuly, přičemž řešte i jejich vzájemnou srážku.
 * Molekuly se mohou pohybovat všemi směry, tedy i diagonálně.
"""

# TODO: Implementujte vaše řešení 
